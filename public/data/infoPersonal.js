window._users = [
    {
        userName: "usuario", 
        pass: "123",   
        firstName:"Paco",
        lastName: "Herber",
        email: "tueres@gmail.com",
        rol: "Administrador",
        area : "Administracion"
    },
    {
        userName: "yomero", 
        pass: "123",   
        firstName:"Hugo",
        lastName: "Patiño",
        email: "yomero@gmail.com",
        rol: "Usuario",
        area : "Contabilidad"
    },
    {
        userName: "luisito", 
        pass: "123",  
        firstName:"Luis",
        lastName: "Omaña",
        email: "luisito@gmail.com",
        rol: "Usuario",
        area : "Medio Ambiente"
    }

];


window._items =  [
    {
        "id": 1,
        "name": "Instituto Tecnológico de Pachuca",
        "url": "http://www.itpachuca.edu.mx/",
        "email": "web_pachuca@tecnm.mx1",
        "phone": "(01 771) 170 03 01",
         "ubicacion": {
            "direccion": "Blvd. Felipe Angeles",
            "no": "s/d",
            "municipio": "Pachuca de Soto",
            "geo": {
                "lat": 20.0832,
                "lng": -98.7738
            }
        },
        "ofertaEducativa": ["Preparatoria","Licenciatura"],
        "ofertaAcademica": ["Ingenieria civil","Arquitectura", "Sistemas informaticos"]

    },
    {
        "id": 2,
        "name": "Tecnologico de Monterrey Campus Pachuca",
        "url": "http://www.itpachuca.edu.mx/",
        "email": "web_pachuca@tecnm.mx2",
        "phone": "(01 771) 170 03 012",
        "ubicacion": {
            "direccion": "Blvd. Felipe Angeles",
            "no": "s/d",
            "municipio": "Pachuca de Soto",
            "geo": {
                "lat": 20.0963,
                "lng": -98.7673
            }
        },
        "ofertaEducativa": ["Preparatoria","Licenciatura","Posgrado","Diplomados"],
        "ofertaAcademica": ["Administracion", "Sistemas informaticos",  "Mecatronica"]
    },
    {
        "id": 3,
        "name": "Universidad Autonoma del Estado de Hidalgo",
        "url": "http://www.itpachuca.edu.mx/",
        "email": "web_pachuca@tecnm.mx3",
        "phone": "(01 771) 170 03 013",
        "ubicacion": {
            "direccion": "Carretera Pachuca - Tulancingo km 4.5 Ciudad del Conocimiento",
            "no": "s/d",
            "municipio": "Pachuca de Soto",
            "geo": {
                "lat": 20.0951,
                "lng": -98.7143
            }
        },
        "ofertaEducativa": ["Preparatoria","Licenciatura","Diplomados","Posgrado"],
        "ofertaAcademica": ["Administracion","Arquitectura", "Ciencias de la salud", "Sistemas informaticos", "Leyes"]
    },
    {
        "id": 4,
        "name": "Universidad del Futbol y Ciencias del Deporte",
        "url": "http://www.itpachuca.edu.mx/",
        "email": "web_pachuca@tecnm.mx4",
        "phone": "(01 771) 170 03 014",
        "ubicacion": {
            "direccion": "Libramiento circuito de la Concepcion",
            "no": "s/d",
            "municipio": "San Agustin Tlaxiaca",
            "geo": {
                "lat": 20.1373,
                "lng": -98.8134
            }
        },
        "ofertaEducativa": ["Licenciatura","Posgrado"],
        "ofertaAcademica": ["Administracion","Ciencias de la salud"]
    },
    {
        "id": 5,
        "name": "Uni​dad Profesional Interdisciplinaria de Ingeniería Campus Hidalgo",
        "url": "https://www.uaeh.edu.mx/",
        "email": "uaeh@tecnm.mx5",
        "phone": "(01 771) 170 03 015",
        "ubicacion": {
            "direccion": "Carretera Pachuca - Actopan km 1.5 Ciudad del Conocimiento y la Cultura",
            "no": "s/d",
            "municipio": "San Agustin Tlaxiaca",
            "geo": {
                "lat": 20.1167,
                "lng": -98.8272
            }
        },
        "ofertaEducativa": ["Pre-escolar","Primaria","Secundaria","Preparatoria","Licenciatura","Diplomados"],
        "ofertaAcademica": ["Sistemas informaticos",  "Mecatronica"]
    },
    {
        id: 6,
        name: 'Instituto de capacitacion para el trabajo',
        email: "web_pachuca@tecnm.mx2",
        phone: "(01 771) 170 03 012",
        url: "http://www.itpachuca.edu.mx/",
        ubicacion: {
            direccion: "Ave. Benito Juarez",
            municipio: "Tulancingo de Bravo",
            geo: {
                lat: 20.0766,
                lng: -98.3704
            }
        },
        "ofertaEducativa": ["Pre-escolar","Primaria","Secundaria"],
        "ofertaAcademica": ["Administracion","Sistemas informaticos", "Mecatronica"]

    },
    {
        id: 7,
        name: 'Instituto de la Administracion Publica',
        email: "web_pachuca@tecnm.mx2",
        phone: "(01 771) 170 03 012",
        url: "http://www.itpachuca.edu.mx/",
        ubicacion: {
            direccion: "Plaza Independencia",
            municipio: "Pachuca de Soto",
            geo: {
                lat: 20.1280,
                lng: -98.7315
            }
        },
        "ofertaEducativa": ["Posgrado"],
        "ofertaAcademica": ["Administracion"]

    }
   
]

window._cataRolUsuario = ['Administrador', 'Usuario'];
window._cataOfertaEducativa = ['Doctorados','Maestrias','Diplomados','Posgrado','Licenciatura','Preparatoria','Secundaria','Primaria','Pre-escolar'];	
window._cataOfertaAcademica =  ['Administracion','Ingenieria civil','Arquitectura','Ciencias de la salud','Sistemas informaticos','Leyes','Mecatronica'];

window._selectOfertaEducativa = ['Diplomados','Posgrado','Licenciatura','Preparatoria','Secundaria','Primaria','Pre-escolar'];	
window._selectOfertaAcademica =  ['Administracion','Ingenieria civil','Arquitectura','Ciencias de la salud','Sistemas informaticos','Leyes','Mecatronica'];



window._arrIconMarkers= {
    'Doctorados':   'Marker_Inside_Pink.png',
    'Maestrias':    'Marker_Inside_Chartreuse.png',
    'Posgrado':     'Marker_Outside_Azure.png',
    'Diplomados':   'Marker_Outside_Chartreuse.png',
    'Licenciatura': 'Marker_Outside_Pink.png',

    'Preparatoria': 'Pin_Left_Azure.png',
    'Secundaria':   'Pin_Left_Chartreuse.png',

    'Primaria':     'PushPin1__Azure.png',
    'Pre-escolar':  'PushPin1__Chartreuse.png',
    '':  'PushPin1__Chartreuse.png',
};

window._arrIcons= {
    'Doctorados':   'fas fa-place-of-worship icon-awesome icon-brown',
    'Maestrias':    'fas fa-place-of-worship icon-awesome icon-brown',
    'Posgrado':     'fas fa-place-of-worship icon-awesome icon-brown',
    'Diplomados':   'fas fa-graduation-cap icon-awesome icon-purple',
    'Licenciatura': 'fas fa-university icon-awesome icon-gray',

    'Preparatoria': 'fas fa-book-reader icon-awesome icon-blue',
    'Secundaria':   'fas fa-book icon-awesome icon-yellow',

    'Primaria':     'fas fa-building icon-awesome icon-orange',
    'Pre-escolar':  'fas fa-book icon-awesome icon-green',
    '':  'fas fa-book icon-awesome icon-green',
};

window._selectMunicipio =  ['San Agustin Tlaxiaca', 'Tulancingo de Bravo','Pachuca de Soto'];

window._cataMunicipio =  [
    'Acatlan',
    'Acaxochitlan',
    'Actopan',
    'Agua Blanca de Iturbide',
    'Ajacuba',
    'Alfajayucan',
    'Almoloya',
    'Apan',
    'Atitalaquia',
    'Atlapexco',
    'Atotonilco de Tula',
    'Atotonilco el Grande',
    'Calnali',
    'Cardonal',
    'Chapantongo',
    'Chapulhuacan',
    'Chilcuautla',
    'Cuautepec de Hinojosa',
    'El Arenal',
    'Eloxochitlan',
    'Emiliano Zapata',
    'Epazoyucan',
    'Estatal',
    'Francisco I. Madero',
    'Huasca de Ocampo',
    'Huautla',
    'Huazalingo',
    'Huehuetla',
    'Huejutla de Reyes',
    'Huichapan',
    'Ixmiquilpan',
    'Jacala de Ledezma',
    'Jaltocan',
    'Juarez Hidalgo',
    'La Mision',
    'Lolotla',
    'Metepec',
    'Metztitlan',
    'Mineral de la Reforma',
    'Mineral del Chico',
    'Mineral del Monte',
    'Mixquiahuala de Juarez',
    'Molango de Escamilla',
    'Nicolas Flores',
    'Nopala de Villagran',
    'Omitlan de Juarez',
    'Pachuca de Soto',
    'Pacula',
    'Pisaflores',
    'Progreso de Alvaro Obregon',
    'San Agustin Metzquititlan',
    'San Agustin Tlaxiaca',
    'San Bartolo Tutotepec',
    'San Felipe Orizatlan',
    'San Salvador',
    'Santiago de Anaya',
    'Santiago Tulantepec de Lugo Guerrero',
    'Singuilucan',
    'Tasquillo',
    'Tecozautla',
    'Tenango de Doria',
    'Tepeapulco',
    'Tepehuacan de Guerrero',
    'Tepeji del Rio de Ocampo',
    'Tepetitlan',
    'Tetepango',
    'Tezontepec de Aldama',
    'Tianguistengo',
    'Tizayuca',
    'Tlahuelilpan',
    'Tlahuiltepa',
    'Tlanalapa',
    'Tlanchinol',
    'Tlaxcoapan',
    'Tolcayuca',
    'Tula de Allende',
    'Tulancingo de Bravo',
    'Villa de Tezontepec',
    'Xochiatipan de Castillo',
    'Xochicoatlan',
    'Yahualica',
    'Zacualtipan de los Angeles',
    'Zapotlan de Juarez',
    'Zempoala',
    'Zimapan'
];