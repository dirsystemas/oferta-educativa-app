import {
    INIT_USERS, 
    GET_USERS, 
    VALIDATE_USER,
    RESET_AUTHO
} from '../actions/types';

const initialState = {
    users: window._users,
    isAuthenticated: null
}

export default function(state=initialState, action){
    switch(action.type){
        case INIT_USERS:
            return {
                ...state,
                users: [...state.users, ...action.users] 
            }
        case GET_USERS:
            return{
                ...state
            }    
        case VALIDATE_USER:
            state.isAuthenticated = false

            state.users.map( (user) => {
                //console.log( item.id +'=='+ action.payload.id)
                if(user.userName === action.payload.userName && user.pass === action.payload.pass){
                    state.isAuthenticated = true
                }
                return{
                    ...state
                }   
            })
        
            return{
                ...state
            } 
        case RESET_AUTHO:
            state.isAuthenticated = null    
            return {
                ...state
            }           
        default:
            return state    
    }
}