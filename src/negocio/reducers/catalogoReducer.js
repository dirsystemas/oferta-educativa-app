import {
    INIT_CATALOGO,
    GET_CATALOGO
} from '../actions/types';

const initialState = {
    cataOfertaEducativa: window._cataOfertaEducativa,
    cataOfertaAcademica: window._cataOfertaAcademica,
    selectOfertaEducativa: window._selectOfertaEducativa,
    selectOfertaAcademica: window._selectOfertaAcademica,
    selectMunicipios: window._selectMunicipio,
    cataMunicipios: window._cataMunicipio,
    cataRolUsuarios: window._cataRolUsuario,
    cataArrIconMarkers: window._arrIconMarkers,
    cataArrIcons: window._arrIcons,
    
}

export default function(state=initialState, action){
   
    switch(action.type){
        case INIT_CATALOGO:
            return {
                ofertaEducativa: [ ...state.ofertaEducativa, ...action.ofertaEducativa] 
            }

        case GET_CATALOGO:
            return{
                ofertaEducativa: [...state.ofertaEducativa]
            }
      
        default:
            return state    
    }
}