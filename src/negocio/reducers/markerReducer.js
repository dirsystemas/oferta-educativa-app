import {
    INIT_MARKERS,
    GET_MARKERS
} from '../actions/types';

const initialState = {
    location:{
        lat: 20.1224,
        lng: -98.73675
    },        
    zoom: 10,
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
    routes: window._routes,
    markers: window._items//[]
}

export default function(state=initialState, action){
   
    switch(action.type){
        case INIT_MARKERS:
            return {
                markers: [ ...state.markers, ...action.markers] //state.items.concat(action.items) 
            }

        case GET_MARKERS:
            return{
                markers: [...state.markers]
            }
      
        default:
            return state    
    }
}