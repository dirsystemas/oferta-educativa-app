import {
    INIT_ITEMS, 
    GET_ITEMS, 
    ADD_ITEM, 
    EDIT_ITEM, 
    DELETE_ITEM,
    GET_ITEM
} from '../actions/types';

const initialState = {
    items: window._items,
    item: {}//[]
}

export default function(state=initialState, action){
   
    switch(action.type){
        case INIT_ITEMS:
            return {
                items: [...state.items, ...action.payload] //...action.payload.items no se ha probado state.items.concat(action.items) 
            }

        case GET_ITEMS:
            return{
               items: [...state.items] //...state
            }
        case  ADD_ITEM:
            return{
               items: [...state.items, action.payload]//action.payload.item no se ha probado
            }  
        case EDIT_ITEM:
            state.items.map( (item) => {
                //console.log( 'action.payload.ofertaEducativa=='+ action.payload.ofertaEducativa)
                if(item.id == action.payload.id){
                    item.name = action.payload.name
                    item.url = action.payload.url
                    item.email = action.payload.email
                    item.phone = action.payload.phone
                    item.ubicacion.direccion = action.payload.ubicacion.direccion
                    item.ubicacion.geo.lat = action.payload.ubicacion.geo.lat
                    item.ubicacion.geo.lng = action.payload.ubicacion.geo.lng
                    item.ubicacion.municipio = action.payload.ubicacion.municipio
                    item.ofertaEducativa = action.payload.ofertaEducativa
                    item.ofertaAcademica = action.payload.ofertaAcademica  
                }
               
            })
            
            return{
                items: [...state.items] 
            }      
        case  DELETE_ITEM:
            //console.log( action.payload)
            return{
                items: state.items.filter(item => item.id !== action.payload)
            }
        case  GET_ITEM:
            //console.log( action.payload)
            return{
                item: state.items.filter(item => item.id === action.payload)
            }    
        default:
            return state    
    }
}