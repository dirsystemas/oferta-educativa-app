import { combineReducers } from 'redux';
import itemReducer from './itemReducer';
import authReducer from './authReducer';
import markerReducer from './markerReducer';
import catalogoReducer from './catalogoReducer';

export default combineReducers({
    itemState: itemReducer,
    authState: authReducer,
    catalogoState: catalogoReducer,
    mapState: markerReducer
});