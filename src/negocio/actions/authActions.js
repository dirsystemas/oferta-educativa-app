import {
    INIT_USERS, 
    GET_USERS,
    VALIDATE_USER,
    RESET_AUTHO
} from './types';

export const initUsers = () => {
    return{
        type: INIT_USERS
    }
}

export const getUsers = () => {
    return{
        type: GET_USERS
    }
}

export const validateUser = user => {
    return{
        type: VALIDATE_USER,
        payload: user
    }
} 

export const resetAuthenticated = () => {
    return {
        type: RESET_AUTHO
    }
}
