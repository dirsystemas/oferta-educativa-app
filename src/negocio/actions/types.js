export const INIT_ITEMS = 'INIT_ITEMS';
export const GET_ITEMS = 'GET_ITEMS';
export const ADD_ITEM = 'ADD_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';
export const GET_ITEM = 'GET_ITEM';

export const INIT_USERS = 'INIT_USERS';
export const GET_USERS = 'GET_USERS';
export const VALIDATE_USER = 'VALIDATE_USER';
export const RESET_AUTHO = 'RESET_AUTHO';

export const INIT_CATALOGO = 'INI_CATALOGO';
export const GET_CATALOGO = 'GET_CATALOGO';

export const INIT_MARKERS = 'INIT_MARKERS';
export const GET_MARKERS = 'GET_MARKERS';