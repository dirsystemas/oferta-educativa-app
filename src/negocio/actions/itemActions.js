import {
    INIT_ITEMS, 
    GET_ITEMS, 
    ADD_ITEM, 
    EDIT_ITEM, 
    DELETE_ITEM,
    GET_ITEM
} from './types';

export const initItems = () => {
    return{
        type: INIT_ITEMS
    }
}

export const getItems = () => {
    return{
        type: GET_ITEMS
    }
}

export const addItem = item => {
    return{
        type: ADD_ITEM,
        payload: item
    }
} 

export const editItem = item => {
    return{
        type: EDIT_ITEM,
        payload: item
    }
}

export const deleteItem = id => {
    return {
        type: DELETE_ITEM,
        payload: id
    }
}

export const getItem = id => {
    return {
        type: GET_ITEM,
        payload: id
    }
}
