import {
    INIT_CATALOGO,
    GET_CATALOGO
} from './types';


export const initCatalogo = () => {
    return{
        type: INIT_CATALOGO
    }
}

export const getCatalogo = () => {
    return{
        type: GET_CATALOGO
    }
}