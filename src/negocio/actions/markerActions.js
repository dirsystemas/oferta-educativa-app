import {
    INIT_MARKERS,
    GET_MARKERS
} from './types';


export const initMarkers = () => {
    return{
        type: INIT_MARKERS
    }
}

export const getMarkers = () => {
    return{
        type: GET_MARKERS
    }
}