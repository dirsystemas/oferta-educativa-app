import React from 'react';
import { Link } from "react-router-dom";

const PageNotFound = () => (
    <main role="main" className="container" style={{marginTop:'150px'}}>
        <h1 className="mt-5">404</h1>
        <p><strong>Pagina no encontrada</strong></p>

        <p>
            El sitio configurado en esta dirección no contiene el archivo solicitado.
        </p>

        <p> <Link  to="/">Regresar a pagina de inicio</Link> </p>
    </main>
)

export default PageNotFound;