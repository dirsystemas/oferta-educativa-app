import React, { Component} from 'react';

class ButtonCustom extends Component{

    
    render(){

        const {
            _id,
            _type,
            _dataDismiss,
            _dataToogle,
            _dataTarget,
            _buttonClassName,
            _buttonStyle,
            _onClick,
            _iconClassName,
            _iconStyle,
            _label,
            _buttonTitle

        } = this.props

        return(
            <button 
                id={_id}
                type={_type}
                data-dismiss={_dataDismiss}
                data-toggle={_dataToogle}
                data-target={_dataTarget}
                className={_buttonClassName}
                style={{_buttonStyle}}
                title={_buttonTitle}
                onClick = {_onClick}
            >
            <i 
                className={_iconClassName} 
                style={{_iconStyle}}
            ></i>&nbsp;&nbsp;{_label}
            </button> 
        )
    }
}

export default ButtonCustom;