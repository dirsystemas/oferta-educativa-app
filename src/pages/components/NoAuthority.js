import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { resetAuthenticated } from '../../negocio/actions/authActions';

class NoAuthority extends Component {
   
    componentDidMount(){
       // console.log('componentDidMount isAuthenticated=', this.props.isAuthenticated)
        this.props.resetAuthenticated()
        
    }

    componentWillMount(){
      //  console.log('componentWillMount isAuthenticated=', this.props.isAuthenticated)
      //  this.props.resetAuthenticated()
        
    }
    

    render() { 
        return ( 
            <main role="main" className="container" style={{marginTop:'150px'}}>
                <h1 className="mt-5">Favor de autentificarse en el sistema.</h1>
                <p></p>

                <p></p>

                <p> <Link  to="/login">Regresar a pagina de autentificacion.</Link> </p>
            </main>
           
         );
    }
}

  
  export default connect(
    null,
    {resetAuthenticated}
  ) (NoAuthority);
  
