import React, { Component } from 'react';

class InputTextCustom extends Component {
    
    render() { 
        const {
            _name,
            _label,
            _type,
            _placeholder,
            _defaultValue,
            _readOnly,
            _onChange,
            _ref,
            _required
        } = this.props

        return ( 
            <div className="form-group">
                <label htmlFor={_name} className="col-form-label">{_label}:</label>
                <input
                    className='form-control'
                    type={_type}
                    id={_name} 
                    name={_name}
                    placeholder={_placeholder}
                    defaultValue={_defaultValue}
                    readOnly={_readOnly}
                    onChange={_onChange}
                    ref={_ref}
                    required={_required}
                />
            </div>

         );
    }
}
 
export default InputTextCustom;