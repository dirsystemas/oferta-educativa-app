import React, {Component} from 'react';
import { Redirect, Link } from "react-router-dom";
import { connect } from 'react-redux';
import { resetAuthenticated } from '../../negocio/actions/authActions';

class NavbarInverse extends Component {
    state = {
        redirectToReferrer: false
      }
   
    _onClick = () => {
        this.props.resetAuthenticated()
        this.setState(
            {redirectToReferrer: true}
          )
      };


    render(){
        const { 
            _displayNavbarInicio,
            _displayButtonRefreshMapa,
            _displayButtonConsultaMapa,
            _displayButtonConsultaEscuelas,
            _displayButtonRegEscuelas,
            _displayButtonRegUsuarios,
            isAuthenticated
        } = this.props

        const _itemInicio = `nav-link ${this.props.itemInicio}`
        const _itemRegEscuelas = `nav-link ${this.props.itemRegEscuelas}`
        const _itemRegUsuarios = `nav-link ${this.props.itemRegUsuarios}`
        const _itemAcercaDe = `nav-link ${this.props.itemAcercaDe}`
        const _itemBuzon = `nav-link ${this.props.itemBuzon}`
        const _itemLogin = `nav-link ${this.props.itemLogin}`

        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/'}/>)
          }

        return(
            <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top  bg-dark" >
                <span className="navbar-brand mb-0 h2" style={_styles.linkTitle} >
                <img src='img/png/ajad.png' width='45px' style={{border:'none'}} alt='icono'/> Oferta Educativa</span>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    {_displayNavbarInicio  ? (
                        //Links para consulta publica
                        <ul className="nav ml-auto mr-auto ml-md-0" >  
                            <li className="nav-item" style={_styles.linkSpanMap}>
	                             { _displayButtonRefreshMapa === 1 ? (
	                                <button
                                        className='btn btn-outline-info'
                                        type='button'
                                        data-toggle="modal" 
	                                    title='Refrescar mapa digital' 
	                                    onClick={this.props.onResetItems}
                                    >Refrescar</button>
	                            ):(null)}   
                            </li> 
                            <li className="nav-item" style={_styles.linkSpanMap}>
	                            { _displayButtonConsultaMapa === 1 ? (
                                    <button
                                        className='btn btn-outline-success'
                                        type='button'
                                        data-toggle="modal" 
                                        data-target="#consultaMapModal" 
                                        title='Consulta escuelas' 
                                        onClick={this.props.onResetItems}
                                    >Consultar</button>
	                            ):(null)} 
                            </li>  
                            <li className="nav-item">
                                <Link className={_itemInicio} to="/">Inicio</Link>
                            </li> 
                            <li className="nav-item">
                                <Link className={_itemBuzon} to="/buzon">Contacto</Link> 
                            </li>
                            <li className="nav-item">
                                <Link className={_itemAcercaDe} to="/about">Acerca de ..</Link>  
                            </li>
                            <li className="nav-item">
                                <Link className={_itemLogin} to="/login">
                                    {isAuthenticated ? 'Administracion' : 'Login'}
                                </Link>
                            </li>
                        </ul>    
                    ) : (  
                        //Links para administracion del sitio
                        <ul className="nav ml-auto mr-auto ml-md-0" > 
                            {  _displayButtonConsultaEscuelas === 1 ? (
                                <li className="nav-item" style={_styles.linkAddSpan}>
                                    <button
                                        className='btn btn-outline-success'
                                        type='button'
                                        data-toggle="modal" 
                                        data-target="#consultaModal" 
                                        title='Consulta registros' 
                                    >Consultar</button>
	                            </li>    
                            ):(null)}
                            { _displayButtonRegEscuelas === 1 ? (  
                                <li className="nav-item" style={_styles.linkAddSpan}>
                                    <button
                                        className='btn btn-outline-info'
                                        type='button'
                                        onClick={this.props.handleAddRecord}
                                        title='Insertar registro' 
                                    >Insertar</button>
                                </li>  
                            ):(null)}      
                            { _displayButtonRegUsuarios === 1 ? (
                                <li className="nav-item" style={_styles.linkAddSpan}>
                                    <button
                                        className='btn btn-outline-info'
                                        type='button'
                                        data-toggle="modal" 
                                        data-target="#formAddUsuarioModal" 
                                        title='Insertar registro' 
                                    >Insertar</button>
                                </li>  
                            ):(null)}
                            <li className="nav-item">
                                <Link className={_itemInicio} to="/">Inicio</Link>
                            </li>
                            <li className="nav-item">
                                <Link className={_itemRegEscuelas} to="/records">Registro Escuelas</Link>
                            </li>
                            <li className="nav-item">
                                <Link className={_itemRegUsuarios} to="/usuarios">Registro Usuarios</Link>
                            </li>
                            <li className="nav-item" style={_styles.linkSpan}>
                                {isAuthenticated ?(
                                    <a 
                                        className="nav-link" 
                                        onClick={this._onClick}
                                        href='#'  
                                        >Logout
                                    </a>
                                ):(null)}
                            </li>  
                         </ul>
                     
                    )}

                </div>
            </nav>
        </header>
        )
    }
}

const _styles = {
    linkTitle:{
        marginRight:'30px'
    },
    linkSpanMap:{
        marginLeft:'20px',
        marginRight:'30px'
    },
    linkAddSpan:{
        marginLeft:'20px',
        marginRight:'30px'
    }
}

const mapStateToProps = state =>({
    isAuthenticated: state.authState.isAuthenticated
  })

export default connect(
    mapStateToProps,
    {resetAuthenticated}
  ) (NavbarInverse);
