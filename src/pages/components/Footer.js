import React, {Component} from 'react';

class Footer extends Component {
    
    render() {
      return (
            <footer className="footer bg-dark" style={{position: 'fixed', bottom: '0px', width:'100%', minHeight:'50px', padding:'5px' }}>
                <div className="container">
                <span className="text-muted">Curso de React - Node. Nivel Intermedio</span>
                </div>
            </footer>
      )
    }
  }
  
  export default Footer;
