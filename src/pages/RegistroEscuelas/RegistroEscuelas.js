import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavbarInverse from '../components/NavbarInverse';
import Footer from '../components/Footer';
import CardEscuela from './components/CardEscuela'
import RecordEditPage from './components/RecordEditPage';
import RecordAddPage from './components/RecordAddPage';

class RegistroEscuelas extends Component {
   
  state = {
    item:{},
    _displayTransOn: true,
    _displayPageEdit: true,
    _displayButtonRegEscuelas: 1,
    _displayButtonConsultaEscuelas: 1,
    _bolEditButton: 1
  }  

  componentDidMount(){
    //console.log('Store=',this.props.mapState.markers.length)
    let element = this.props.itemState.items[0]
    this.setState({
        item: this.props.itemState.items[0],
        _displayTransOn: true,
        _displayPageEdit: true,
        _displayButtonRegEscuelas: 1,
        _displayButtonConsultaEscuelas: 1,
    })
  }
  
  //llamado desde CardEscuela.js para actualizacion en RecordEditModal
  _handleEditRecord=(item)=>{
    this.setState({ 
      item,
      _displayTransOn: false,
      _displayPageEdit: true,
      _displayButtonRegEscuelas: 0,
      _displayButtonConsultaEscuelas: 0,
    });
 
  }

  _handleAddRecord=()=>{
    this.setState({ 
      _displayTransOn: false,
      _displayPageEdit: false,
      _displayButtonRegEscuelas: 0,
      _displayButtonConsultaEscuelas: 0,
    });
 
  }

  _handleClose=()=>{
    this.setState({ 
      _displayTransOn: true,
      _displayPageEdit: true,
      _displayButtonRegEscuelas: 1,
      _displayButtonConsultaEscuelas: 1,
    });
}
 
  render() {

    const {
      _displayTransOn,
      _displayPageEdit,
      _displayButtonRegEscuelas,
      _displayButtonConsultaEscuelas,
      _bolEditButton
    }= this.state
   
    return (
        <div>
            <NavbarInverse 
                handleAddRecord={this._handleAddRecord}
                itemRegEscuelas='disabled'
                _displayButtonRegEscuelas= {_displayButtonRegEscuelas}
                _displayButtonConsultaEscuelas = {_displayButtonConsultaEscuelas}
            />

            <div className="box" style={_styles.box} >
                { _displayTransOn ? (
                  <div  style={_styles.container}>
                      <h1 className="mt-5">Registro Escuelas</h1> 
                      <CardEscuela 
                        handleEditRecord = {this._handleEditRecord}
                        _bolEditButton = {_bolEditButton}
                      />
                  </div>

                ): (
                  <div  style={_styles.container}>
                    { _displayPageEdit ? (
                      <RecordEditPage 
                        item={this.state.item}
                        _onClose = {this._handleClose}
                      />
                    ): (
                      <RecordAddPage
                        _onClose = {this._handleClose}
                      />
                    )}
                  </div>
                )}
                
            </div>

            <Footer />
        </div>
    )
  }
}

const _styles = {
  container:{
      marginLeft:'30px'
      ,marginRigth: '30px'
      ,width:'95%'
  }
  ,box:{
    marginTop:'80px'
    , marginBottom:'60px'
  }
}

const mapStateToProps = state =>({
  itemState: state.itemState
})

export default connect(
  mapStateToProps,
  null
)(RegistroEscuelas);
