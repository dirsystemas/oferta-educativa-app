import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getItems } from '../../../negocio/actions/itemActions';

import ButtonCustom from '../../components/ButtonCustom';
import ConsultaModal from './ConsultaModal';

class CardEscuela extends Component {

    state ={
        items:[],
        prevParam: '',
        prevNivelQry:0,
        arrQuerys: ['', 'Oferta educativa', 'Oferta academica', 'Municipio']
    }

    componentDidMount() {
        this.props.getItems();//no quitarlo
        this.setState({
            items: this.props.itemState.items
        })
    }

    _onResetItems = () => {
        this.setState({
            items: this.props.itemState.items,
            prevParam: '',
            prevNivelQry:0
        })
    }

    _onHandleOfertaEducativa = param => {
        if(!param)
            return

        this.setState({
            items: this.props.itemState.items.filter(item => item.ofertaEducativa.find( (element) => element === param ) )        
        })
    }

    _onHandleOfertaAcademica = param => {
        if(!param)
            return

        this.setState({
            items: this.props.itemState.items.filter(item => item.ofertaAcademica.find( (element) => element === param ) )        
        })
    }

    _onHandleMunicipio = param => {
        if(!param)
            return

        this.setState({
            items: this.props.itemState.items.filter(item=> item.ubicacion.municipio && item.ubicacion.municipio === param )       
        })
    }
    
    render(){
        const { 
            items 
        } = this.state
       
        const { 
            cataArrIcons 
        } = this.props.catalogoState

        return(
            <div>
                <ConsultaModal 
                    onResetItems = {this._onResetItems}
                    onHandleOfertaEducativa={this._onHandleOfertaEducativa.bind(this)}
                    onHandleOfertaAcademica={this._onHandleOfertaAcademica.bind(this)}
                    onHandleMunicipio = {this._onHandleMunicipio.bind(this)}      
                />            
                <div className='card-container d-flex flex-wrap justify-content-around align-items-top'>
                    {items.map( (item, i) => (
                        <div className='card'  key={i} >
                            <div className="card-header text-center">
                                {item.ofertaEducativa && item.ofertaEducativa.length > 0 && item.ofertaEducativa[0].length > 0 ? (
                                    <i className={cataArrIcons[item.ofertaEducativa.slice(-1).pop()] } ></i>
                                ) : (
                                    <i className='fas fa-book icon-awesome icon-gray'></i>
                                )}
                                
                                <div className="title">
                                    <h6>{item.name}</h6>
                                </div>
                            </div>
                            <div className="card-body" style={{paddingTop:'2px',paddingBottom:'2px'}}>                
                                <div className='card-text'  style={{width:'330px', height:'150px', overflow:'auto'}} >
                                    <span className='cardEscuela-label'>Sitio:</span>{item.url}<br/>
                                    <span className='cardEscuela-label'>Email:</span>{item.email}<br/>
                                    <span className='cardEscuela-label'>Telefono:</span>{item.phone}<br/>
                                    <span className='cardEscuela-label'>Municipio:</span>{item.ubicacion.municipio}<br/>
                                    <span className='cardEscuela-label'>Direccion:</span>{item.ubicacion.direccion}<br/>
                                    <span className='cardEscuela-label'>Geolocalizacion:</span>lat: {item.ubicacion.geo.lat}<br/>  
                                    <span style={{marginRight:'123px'}}> </span>lng: {item.ubicacion.geo.lng}<br/>
                                    <span className='cardEscuela-label'>Oferta educativa:</span>
                                    { item.ofertaEducativa ? (
                                        <ul>
                                        {item.ofertaEducativa.sort().map( (element, i) =>(
                                            <li key={i}> {element}</li>
                                        ))}
                                    </ul>
                                    ) : (
                                        ''
                                    )}
                                    <span className='cardEscuela-label'>Oferta academica:</span>
                                    <ul>
                                        {item.ofertaAcademica.sort().map( (element, i) =>(
                                            <li key={i}> {element}</li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                            <div className="card-footer text-center">  
                                <p>  
                                    <a className="card-link" 
                                        href={item.url} 
                                        target="_blank" 
                                        rel="noopener noreferrer" 
                                    >Ir al sitio ...</a>
                                </p>
                                {this.props._bolEditButton===1 ? (
                                    <div className="text" style={_styles.text}>
                                        <ButtonCustom 
                                            _dataToogle='modal'
                                            _dataTarget='#formEditModal'
                                            _buttonClassName='btn btn-outline-info'
                                            _buttonStyle= "minWidth:'180px', margin:'10px'"
                                            _label = 'Editar'
                                            _iconClassName= 'fa fa-info-circle fa-1x'
                                            _iconStyle= "color:'#0000FF'"
                                            _onClick=  { ()=>this.props.handleEditRecord(item) }
                                        />
                                    
                                    </div>  
                                ):(null)}
                                        
                            </div>
                        </div>        
                    ))}
                
                </div>	     
            </div>
            
        )

    }

}

const _styles = {
    text:{
        marginBottom:'15px'
    }
}

const mapStateToProps = state =>({
    itemState: state.itemState,
    catalogoState: state.catalogoState
  })

export default connect(
    mapStateToProps,
    {getItems}
  )(CardEscuela);