import React, { Component } from 'react';
import {connect} from 'react-redux';
import ButtonCustom from '../../components/ButtonCustom';

class ConsultaModal extends Component{
  
    _onReset = () =>{
        this.props.onResetItems()
        this.formConsultaModal.reset()
    }

    render(){
        const {
            selectOfertaEducativa,
            selectOfertaAcademica,
            selectMunicipios
        } = this.props.catalogoState

        return(
            <div className="modal fade" id="consultaModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Consulta</h5>
                            <span style={{marginLeft:'50px'}}></span>
                            <ButtonCustom 
                                _type='reset'
                                _buttonClassName='btn  btn-outline-info btn-space-40'
                                _dataDismiss='modal'
                                _buttonStyle= {_styles.button}
                                _label = 'Reset'
                                _iconClassName= 'fas fa-undo'
                                _buttonTitle = 'Refrescar informacion'
                                _onClick={this._onReset}
                            />
                            <span style={{marginLeft:'30px'}}></span>
                            <ButtonCustom 
                                _type='button'
                                _buttonClassName='btn  btn-outline-success btn-space-40'
                                _dataDismiss='modal'
                                _buttonStyle= {_styles.button}
                                _label = 'Cerrar'
                                _buttonTitle = 'Cerrar dialogo'
                                _iconClassName= 'fas fa-times-circle'
                            />
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style={{fontSize:'36px', color:'red'}}>&times;</span>
                            </button>
                        </div>
                        <form ref={(el) => this.formConsultaModal = el}>
                            <div className="modal-body modal-color">
                                <div className="form-group">
                                    <p>Seleccionar Oferta educativa:</p>
                                    <select 
                                        name="reg_ofertaEducativa" 
                                        className="custom-select mb-3" 
                                        data-dismiss='modal'
                                        size='5'
                                        onChange = { ( { target: {value}  } ) => {
                                            this.props.onHandleOfertaEducativa(value) 
                                            this.formConsultaModal.reset()
                                    }}
                                    > 
                                        {selectOfertaEducativa.sort().map( (element, i) =>(
                                            <option value={element} key={i}>{element}</option>
                                        ))}
                                    </select>
                                
                                    <label>Seleccionar Oferta academica:</label>
                                    <select 
                                        name="reg_ofertaAcademica" 
                                        className="custom-select mb-3" 
                                        data-dismiss='modal'
                                        size='5'
                                        onChange = { ( { target: {value}  } ) => {
                                            this.props.onHandleOfertaAcademica(value) 
                                            this.formConsultaModal.reset()
                                    }}
                                    > 
                                    {selectOfertaAcademica.sort().map( ( element, i ) => (
                                        <option value={element} key={i}>{element}</option>
                                    ))}
                                    </select>
                            
                                    <label >Seleccionar Municipio:</label>
                                    <select 
                                        name="reg_municipio" 
                                        className="custom-select mb-3" 
                                        data-dismiss='modal'
                                        size='5'
                                        onChange = { ( { target: {value}  } ) => {
                                            this.props.onHandleMunicipio(value) 
                                            this.formConsultaModal.reset()
                                    }}
                                    >
                                    {selectMunicipios.sort().map( (element, i) =>(
                                        <option value={element} key={i}>{element}</option>
                                    ))}
                                    </select>
                                </div>
                            </div>
                            <div className="modal-footer">
                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const _styles = {
    button:{
        minWidth:'80px',
        marginLeft:'40px',
        marginRight:'40px'
    }
}
  
const mapStateToProps = state => ({
    catalogoState: state.catalogoState
})

export default connect(
    mapStateToProps,
    null
)(ConsultaModal);

