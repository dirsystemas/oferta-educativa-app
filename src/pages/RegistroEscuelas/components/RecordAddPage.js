import React, {Component} from 'react';
import { connect } from 'react-redux';
import { addItem } from '../../../negocio/actions/itemActions';
import uuid from 'uuid';

import InputTextCustom from '../../components/InputTextCustom';
import ButtonCustom from '../../components/ButtonCustom';

class RecordAddPage extends Component{
    state = {
        name: '',
        url: '',
        email: '',
        phone: '',
        direccion: '',
        municipio: '',
        lat: '',
        lng: '',
        ofertaEducativa: [],
        ofertaAcademica: []
      };

      _onChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    _onChangeOfertaEducativa = e => {
        var opts = [], opt
        let select = document.getElementById('ofertaEducativa_add')
       
        for(let i=0; i < select.options.length; i++){
            opt = select.options[i]
            if(opt.selected){
                opts.push(opt.value);
            }
        }
     
        this.setState({
            ofertaEducativa: opts
        })
    }

    _onChangeOfertaAcademica = e => {
        var opts = [], opt
        let select = document.getElementById('ofertaAcademica_add')
      
        for(let i=0; i < select.options.length; i++){
            opt = select.options[i]
            if(opt.selected){
                opts.push(opt.value);
            }
        }
        this.setState({
            ofertaAcademica: opts
        })
    }
    
    _onUpdate = e => {
        // e.preventDefault();
       // console.log('RecordAddModal  _onUpdate ofertaEducativa = ',this.state.ofertaEducativa )
       // console.log('RecordAddModal  _onUpdate ofertaAcademica = ',this.state.ofertaAcademica )
         const newItem = {
             id: uuid(),
             name: this.state.name,
             url:  this.state.url,
             email: this.state.email,
             phone: this.state.phone,
             ubicacion: {
                 direccion: this.state.direccion,
                 municipio: this.state.municipio,
                 geo: {
                     lat: parseFloat(this.state.lat),
                     lng: parseFloat(this.state.lng)
                 }
             },
             ofertaEducativa: this.state.ofertaEducativa,
             ofertaAcademica: this.state.ofertaAcademica
         };
     
         this.props.addItem(newItem)
         this.props._onClose()
     };
 
     _onClose = () =>{
         this.props._onClose()
     }

    
    render(){
        const {
            cataOfertaEducativa,
            cataOfertaAcademica,
            cataMunicipios
        } = this.props.catalogoState

        return(
            <div  id="formAddpage" >
                <div className="modal-header" style={_styles.colorHeader}>
                    <h5 >Nuevo Registro </h5>
                    <span style={{marginLeft:'130px'}}></span>
                    <ButtonCustom 
                        _type='button'
                        _buttonClassName='btn  btn-outline-secondary btn-space-40'
                        _dataDismiss='modal'
                        _buttonStyle= {_styles.button}
                        _label = 'Cancelar'
                        _iconClassName= 'fas fa-undo'
                        _onClick={this._onClose}
                    />
                    <span style={{marginLeft:'30px'}}></span>
                    <ButtonCustom 
                        _type='button'
                        _buttonClassName='btn  btn-outline-success btn-space-40'
                        _dataDismiss='modal'
                        _buttonStyle= {_styles.button}
                        _label = 'Actualizar'
                        _iconClassName= 'fas fa-check'
                        _iconStyle= "color:'#008000'"
                        _onClick={this._onUpdate}
                    />       
                    <button type="button" className="close" onClick={this._onClose}  aria-label="Close">
                        <span aria-hidden="true" style={{fontSize:'36px', color:'red'}}>&times;</span>
                    </button>
                </div>

                <div className="row" style={{paddingTop:'5px'}}>
                    <div className="col-xs-6 col-sm-6 col-lg-6" >
                        <div className="modal-body">
                            <InputTextCustom 
                                _type = 'text'
                                _name = 'name'
                                _label= 'Nombre Institucion' 
                                _placeholder = 'Ingrese un nombre'
                                _onChange={this._onChange}
                            />
                            <InputTextCustom  
                                _type = 'email'
                                _name = 'email'
                                _label= 'Correo electronico' 
                                _placeholder = 'Ingrese un correo electronico'
                                _onChange={this._onChange}
                            />
                            
                            <label >Municipio</label>
                            <select
                                className="custom-select mb-3" 
                                name="municipio" 
                                onChange={this._onChange}
                            >
                                <option value='' style={{display:'none'}}>Seleccione un valor</option>
                                {cataMunicipios.sort().map( (element, i) =>(
                                    <option 
                                        value={element} 
                                        key={i}
                                    >{element}</option>
                                ))}
                            </select>
                            <InputTextCustom  
                                _type = 'text'
                                _name = 'direccion'
                                _label= 'Direccion' 
                                _placeholder = 'Ingrese una direccion'
                                _onChange={this._onChange}
                            />
                            <InputTextCustom  
                                _type = 'text'
                                _name = 'phone'
                                _label= 'Telefono(s)' 
                                _placeholder = 'Ingrese numero(s) telefono(s)'
                                _onChange={this._onChange}
                            />        
                        </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-lg-6" >
                        <div className="modal-body">
                            <InputTextCustom  
                                _type = 'text'
                                _name = 'url'
                                _label= 'Sitio web' 
                                _placeholder = 'Ingrese url'
                                _onChange={this._onChange}
                            />  
                            <label className="col-form-label">Georeferencia:</label><br/>
                            <label style={{marginRight:'10px'}}>Latitud:</label>
                            <input 
                                type = 'text'
                                name = 'lat'
                                placeholder = 'Ingrese latitud'
                                onChange={this._onChange}
                                style={_styles.input120}
                            />
                            <span style={{marginLeft:'30px'}}></span>
                            <label style={{marginRight:'10px'}}>Longitud:</label>
                            <input 
                                type = 'text'
                                name = 'lng'
                                placeholder = 'Ingrese longitud'
                                onChange={this._onChange}
                                style={_styles.input120}
                            />
                            <p>Seleccionar Oferta educativa:<span style={_styles.spanBlue}>(*)</span></p>
                            <select 
                                id='ofertaEducativa_add' 
                                name='ofertaEducativa' 
                                className="custom-select mb-3" 
                                size='5'
                                multiple
                                onChange={this._onChangeOfertaEducativa}
                            > 
                                {cataOfertaEducativa.sort().map( (element, i) =>(
                                    <option value={element} key={i}>{element}</option>
                                ))}
                            </select>
                        
                            <label>Seleccionar Oferta academica:<span style={_styles.spanBlue}>(*)</span></label>
                            <select 
                                id='ofertaAcademica_add' 
                                name="ofertaAcademica" 
                                className="custom-select mb-3" 
                                size='6'
                                multiple
                                onChange={this._onChangeOfertaAcademica}
                            > 
                            {cataOfertaAcademica.sort().map( ( element, i ) => (
                                <option value={element} key={i}>{element}</option>
                            ))}
                            </select>            
                            <span style={_styles.spanBlue}>(*) Catalogo de seleccion multiple: Mantener pulsada la tecla Ctrl, para seleccionar varias opciones.</span>              
                        </div>
                    </div>    
                </div>

                <div className="modal-footer">
                                     
                </div>         
            </div>
        )
        
    }
}

const _styles = {
    button:{
        minWidth:'80px',
        marginLeft:'20px'
    },
    input120:{
        width:'128px',
        paddingLeft: '5px'
    }, 
    spanBlue:{
        fontWeight:'bold', 
        color:' #009'
    }
}

const mapStateToProps = state => ({
    item: state.item,
    isAuthenticated: state.authState.isAuthenticated,
    catalogoState: state.catalogoState
});
  
  export default connect(
    mapStateToProps,
    { addItem }
  )(RecordAddPage);

