import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { editItem, deleteItem } from '../../../negocio/actions/itemActions';
import InputTextCustom from '../../components/InputTextCustom';
import ButtonCustom from '../../components/ButtonCustom';


const nameRef = createRef()
const emailRef = createRef()
const urlRef = createRef()
const phoneRef = createRef()
const direccionRef = createRef()
const municipioRef = createRef()
const latitudRef = createRef()
const longitudRef = createRef()

class RecordEditPage extends Component {
    
    _handleClose(){
        this.props._onClose()
    }

    _handleOfertaEducativa() {
        var opts = [], opt
        let select = document.getElementById('ofertaEducativa_edit')
       
        for(let i=0; i < select.options.length; i++){
            opt = select.options[i]
            if(opt.selected){
                opts.push(opt.value);
            }
        }
        return opts
    }

    _handleOfertaAcademica(){
        var opts = [], opt
        let select = document.getElementById('ofertaAcademica_edit')
      
        for(let i=0; i < select.options.length; i++){
            opt = select.options[i]
            if(opt.selected){
                opts.push(opt.value);
            }
        }
        return opts
    }
    
    _onUpdate = e => {
        let _ofertaEducativa = this._handleOfertaEducativa()
        let _ofertaAcademica = this._handleOfertaAcademica()
        const newItem = {
             id: this.refs.idRef.value,
             name: nameRef.current.value,
             url:  urlRef.current.value,
             email: emailRef.current.value,
             phone: phoneRef.current.value,
             ubicacion: {
                direccion: direccionRef.current.value,
                municipio: municipioRef.current.value,
                geo: {
                    lat: parseFloat(latitudRef.current.value), 
                    lng: parseFloat(longitudRef.current.value) 
                }
             },
            ofertaEducativa: _ofertaEducativa,
            ofertaAcademica: _ofertaAcademica
        };
        console.log('RecordEditModal _onUpdate',newItem);
        this.props.editItem(newItem)
        this.props._onClose()
        
     };


    _onDelete = id => {
        if( window.confirm('Se eliminara definitivamente este registro Desea continuar?') ){
            this.props.deleteItem(id)
        }
        this.props._onClose()
    };

    render() { 
    
        const {
            cataOfertaEducativa,
            cataOfertaAcademica,
            cataMunicipios
        } = this.props.catalogoState

        const { 
            id,
            name, 
            url, 
            email,
            phone, 
            ubicacion,
            ofertaEducativa,
            ofertaAcademica
        } = this.props.item

        return ( 
 
            <div  id="formEditModal" >
                <div className="modal-header" style={_styles.colorHeader}>
                    <h5 >Editar Registro </h5>
                    <span style={{marginLeft:'130px'}}></span>
                    <ButtonCustom 
                        _type='button'
                        _buttonClassName='btn  btn-outline-danger mr-6'
                        _dataDismiss='modal'
                        _buttonStyle= {_styles.button}
                        _label = 'Eliminar '
                        _iconClassName= 'fas fa-times fa-1x'
                        _iconStyle= "color:'#0000FF'"
                        _onClick=  { this._onDelete.bind(this, id) }
                    />
                    <span style={{marginLeft:'30px'}}></span>
                    <ButtonCustom 
                        _type='button'
                        _buttonClassName='btn  btn-outline-secondary mr-6'
                        _dataDismiss='modal'
                        _buttonStyle= {_styles.button}
                        _label = 'Cancelar'
                        _iconClassName= 'fas fa-undo'
                        _onClick=  {this._handleClose.bind(this)}
                    />
                    <span style={{marginLeft:'30px'}}></span>
                    <ButtonCustom 
                        _type='button'
                        _buttonClassName='btn  btn-outline-success mr-6'
                        _buttonStyle= {_styles.button}
                        _label = 'Actualizar'
                        _iconClassName= 'fas fa-check'
                        _iconStyle= "color:'#008000'"
                        _onClick=  { this._onUpdate }
                    />                
                    <button type="button" className="close" onClick={this._handleClose}  aria-label="Close">
                        <span aria-hidden="true" style={{fontSize:'36px', color:'red'}}>&times;</span>
                    </button>
                </div>
        
                <div className="row" style={{paddingTop:'5px'}}>
                    <div className="col-xs-6 col-sm-6 col-lg-6" >
                        <div className="modal-body">
                            <InputTextCustom
                                _type = 'text'
                                _name = 'name'
                                _label= 'Nombre Institucion' 
                                _placeholder = 'Ingrese un nombre'
                                _defaultValue={name}
                                _ref={nameRef}
                            />            
                            <InputTextCustom
                                _type = 'email'
                                _name = 'email'
                                _label= 'Correo electronico' 
                                _placeholder = 'Ingrese un correo electronico'
                                _defaultValue={email}
                                _ref={emailRef}
                            />            
                            <label >Municipio </label>
                            <select
                                className="custom-select mb-3" 
                                name="municipio" 
                                defaultValue={ubicacion.municipio}
                                onChange={this._onChange}
                                ref={municipioRef}
                            >   
                                {cataMunicipios.sort().map( (element, i) =>(
                                    (ubicacion.municipio === element) ? (
                                        <option value={element} selected key={i}>{element}</option>
                                    ) : (
                                        <option value={element} key={i}>{element}</option>
                                    )
                                ))}    
                            </select>     
                            <InputTextCustom  
                                _type = 'text'
                                _name = 'direccion'
                                _label= 'Direccion' 
                                _placeholder = 'Ingrese una direccion'
                                _defaultValue={ubicacion.direccion}
                                _ref={direccionRef}
                            />      
                            <InputTextCustom
                                _type = 'text'
                                _name = 'phone'
                                _label= 'Telefono(s)' 
                                _placeholder = 'Ingrese Numero(s) telefono(s)'
                                _defaultValue={phone}
                                _ref={phoneRef}
                            />              
                        </div>    
                    </div>
                    <div className="col-xs-6 col-sm-6 col-lg-6" >
                        <div className="modal-body">
                            <InputTextCustom 
                                _type = 'text'
                                _name = 'url'
                                _label= 'Sitio web' 
                                _placeholder = 'Ingrese url'
                                _defaultValue={url}
                                _ref={urlRef}
                            /> 
                            <label className="col-form-label">Georeferencia:</label><br/>
                            <label style={{marginRight:'10px'}}>Latitud:</label>
                            <input 
                                type = 'text'
                                name = 'lat'
                                defaultValue={ubicacion.geo.lat}
                                placeholder = 'Ingrese latitud'
                                ref={latitudRef}
                                style={_styles.input120}
                            />
                            <span style={{marginLeft:'30px'}}></span>
                            <label style={{marginRight:'10px'}}>Longitud:</label>
                            <input 
                                type = 'text'
                                name = 'lng'
                                defaultValue={ubicacion.geo.lng}
                                placeholder = 'Ingrese longitud'
                                ref={longitudRef}
                                style={_styles.input120}
                            /><br/>
                            <label>Seleccionar Oferta educativa:<span style={_styles.spanBlue}>(*)</span></label>
                            <select 
                                id='ofertaEducativa_edit'
                                name="reg_ofertaEducativa" 
                                className="custom-select mb-3" 
                                size='5'
                                multiple
                            > 
                                {cataOfertaEducativa.sort().map( (element, i) =>(
                                    ( ofertaEducativa.find( item => item === element) ) ? (
                                        <option value={element} selected key={i}>{element}</option>
                                    ) : (
                                        <option value={element} key={i}>{element}</option>
                                    )
                                ))}
                            </select>
                            <label>Seleccionar Oferta academica:<span style={_styles.spanBlue}>(*)</span>  </label>
                            <select 
                                id='ofertaAcademica_edit'
                                name="reg_ofertaAcademica" 
                                className="custom-select mb-3" 
                                size='6'
                                multiple
                        
                            > 
                            {cataOfertaAcademica.sort().map( ( element, i ) => (
                                ( ofertaAcademica.find( item => item === element) ) ? (
                                    <option value={element} selected key={i}>{element}</option>
                                ) : (
                                    <option value={element} key={i}>{element}</option>
                                )
                            ))}
                            </select>  
                            <span style={_styles.spanBlue}>(*) Catalogo de seleccion multiple: Mantener pulsada la tecla Ctrl, para seleccionar varias opciones.</span>  
                        </div>
                    </div>    
                </div>
                <div className="modal-body">
                    <label style={{fontWeight:'bold',marginRight:'10px'}}>Id:</label>
                    <input 
                        type = 'text'
                        name = 'id'
                        label='Id:++'
                        defaultValue={id}
                        readOnly='readOnly'
                        ref='idRef'
                        style={_styles.input350}
                    />
                    
                </div>                      
            </div>

         )
    }
}

const _styles = {
    button:{
        minWidth:'80px',
        marginLeft:'140px'
    },
    input350:{
        width:'350px',
        backgroundColor:'#EFF5F5'
    },
    input120:{
        width:'128px',
        paddingLeft: '5px'
    }, 
    spanBlue:{
        fontWeight:'bold', 
        color:' #009'
    },
    colorHeader:{
        backgroundColor:'#fff'
    }
}
 
const mapStateToProps = state => ({
    catalogoState: state.catalogoState
});

export default connect(
    mapStateToProps,
    {editItem, deleteItem}
  )(RecordEditPage);

