import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { validateUser } from '../../negocio/actions/authActions';

import NavbarInverse from '../components/NavbarInverse';
import Footer from '../components/Footer';
import InputTextCustom from '../components/InputTextCustom';
import ButtonCustom from '../components/ButtonCustom';

class Login extends Component {
  state = {
    pass: '',
    userName:'',
    isAuthenticated: false
  }

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  _onLogin = e => {
     const newItem = {
        pass: this.state.pass,
        userName: this.state.userName
     };
 
     this.props.validateUser(newItem)
       
 };

 
  render() {
  
    if (this.props.isAuthenticated===true) {
        return (<Redirect to={'/records'}/>)
    }else if(this.props.isAuthenticated===false){
      return (<Redirect to={'/noAuthority'}/>)
    }

    return (
        <div>
            <NavbarInverse 
                itemLogin='disabled'
                _displayNavbarInicio = 'true'
            />
            <main role="main" className="container" style={_styles.main}>
              <form className="form-signin" >
                  <div className="text-center mb-4">
                      <img className="mb-4" src="img/png/usuario.png" alt="" style={_styles.imagen} />
                      <h1 className="h3 mb-3 font-weight-normal">Autentificación</h1>
                     
                  </div>
                  <InputTextCustom 
                      _type = 'text'
                      _name = 'userName'
                      _label= 'Usuario' 
                      _placeholder = 'Ingrese nombre usuario: usuario'
                      _required='required'
                      _onChange={this._onChange}
                  />
                  <InputTextCustom  
                      _type = 'password'
                      _name = 'pass'
                      _label= 'Conraseña' 
                      _placeholder = 'Ingrese una constraseña: 123'
                      _required='required'
                      _onChange={this._onChange}
                  />
                  
                  <ButtonCustom 
                    _type='button'
                    _buttonClassName='btn  btn-outline-success btn-space-40'
                    _label = 'Aceptar'
                    _iconClassName= 'fas fa-check'
                    _onClick={this._onLogin}
                  />
                  <span style={{marginLeft:'30px'}}></span>
                  <ButtonCustom 
                    _type='reset'
                    _buttonClassName='btn  btn-outline-secondary btn-space-40'
                    _label = '    Reset&nbsp;&nbsp;&nbsp;&nbsp;'
                    _iconClassName= 'fas fa-redo-alt'
                  />
                  <p className="mt-5 mb-3 text-muted text-center">&copy; Curso React - Node Nivel Intermedio 2019 </p>
              </form>
            </main>
           
            <Footer />
        </div>
    )
  }
}

const _styles = {
  main:{
    marginTop:'80px',
    width: '500px'
  },
  imagen:{
    padding:'15px',
    maxWidth:'35%', 
    border:'6px solid #444747', 
    borderRadius:'50%'
  }
}

const mapStateToProps = state =>({
  isAuthenticated: state.authState.isAuthenticated
})

export default connect(
  mapStateToProps,
  {validateUser}
) (Login);
