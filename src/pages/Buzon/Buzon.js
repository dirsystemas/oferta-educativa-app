import React, { Component } from 'react';

import NavbarInverse from '../components/NavbarInverse';
import Footer from '../components/Footer';

class Buzon extends Component {
    
  render() {
    
        
    return (
        <div>
            <NavbarInverse 
                itemBuzon='disabled'
                _displayNavbarInicio = 'true'
            />
            <main role="main" className="container" style={{marginTop:'150px'}}>
              <h1 className="mt-5">Pagina  Buzon</h1>
            </main>
           
            <Footer />
        </div>
    )
  }
}

export default Buzon;