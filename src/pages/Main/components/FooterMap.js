import React, {Component} from 'react';

class FooterMap extends Component {
    
    render() {
      return (
            <footer className="footer bg-dark" style={{position: 'fixed', bottom: '0px', width:'100%', minHeight:'50px', padding:'5px',opacity:'0.85' }}>
                <div  style={{width:'100%', margin:'10px 10px 10px 10px' }}>

                  <span className="text-muted"><b>Simbologia de los marcadores :</b>   Doctorados:</span>
                  <img src='img/markers/Marker_Inside_Pink.png' width='25px' alt='icono'/>
                  <span className="text-muted">      Maestrias:</span>
                  <img src='img/markers/Marker_Inside_Chartreuse.png' width='25px' alt='icono'/>
                  <span className="text-muted">      Posgrados:</span>
                  <img src='img/markers/Marker_Outside_Azure.png' width='25px' alt='icono'/>
                  <span className="text-muted">     Diplomados:</span>
                  <img src='img/markers/Marker_Outside_Chartreuse.png' width='25px' alt='icono'/>
                  <span className="text-muted">     Licenciatura:</span>
                  <img src='img/markers/Marker_Outside_Pink.png' width='25px' alt='icono'/>
                
                  <span className="text-muted">     Preparatoria:</span>
                  <img src='img/markers/Pin_Left_Azure.png' width='25px' alt='icono'/>
                  <span className="text-muted">     Secundaria:</span>
                  <img src='img/markers/Pin_Left_Chartreuse.png' width='25px' alt='icono'/>

                  <span className="text-muted">     Primaria:</span>
                  <img src='img/markers/PushPin1__Azure.png' width='25px' alt='icono'/>
                  <span className="text-muted">     Pre-escolar:</span>
                  <img src='img/markers/PushPin1__Chartreuse.png' width='25px' alt='icono'/>
                </div>
            </footer>
      )
    }
  }
  
  export default FooterMap;