import React, { Component } from 'react';
class CardInfoWindow extends Component {
    state = {  }
    render() { 
        const {
            icon,
            name,
            phone,
            email,
            municipio,
            direccion,
            url
        } = this.props

        return ( 
            <div className="card" >
                <div className="card-header text-center">
                    <i className={icon} style={{fontSize:'20px'}}></i>
                </div>
                <div className="card-body text-left">
                    <h6 className="card-title">{name}</h6>
                    <p className="card-text"><span className='card-label'>Telefono:</span>{phone}</p>
                    <p className="card-text"><span className='card-label'>Email:</span>{email}</p>
                    <p className="card-text"><span className='card-label'>Municipio:</span>{municipio}</p>
                    <p className="card-text"><span className='card-label'>Direccion:</span>{direccion}</p>
                </div>
                <div className="card-footer">
                    <a href= {url}  target="_blank" rel="noopener noreferrer" className="card-link">Ir al sitio</a>
                </div>
        
            </div>            
         );
    }
}
 
export default CardInfoWindow;