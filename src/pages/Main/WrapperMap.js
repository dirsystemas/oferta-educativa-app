import React, {Component} from 'react';
import { connect } from 'react-redux';
import L from 'leaflet';
import { 
    Map, 
    TileLayer, 
    Marker, 
    Popup,
    ZoomControl,
    Polyline
} from 'react-leaflet'
import CardInfoWindow from './components/CardInfoWindow';
import NavbarInverse from '../components/NavbarInverse';
import FormConsultaModal from './components/FormConsultaModal';
import FooterMap from './components/FooterMap';


class WrapperMap extends Component{
    state = {
        bounds: [[19.42727, -99.9814], [21.4388, -97.9379]],
        location:{
            lat: 20.1224,
            lng: -98.73675
        },        
        zoom: 13,
        markers: [],
        prevParam: '',
        prevNivelQry:0,
        arrQuerys: ['', 'Oferta educativa', 'Oferta academica', 'Municipio']
    }

    componentDidMount(){
      //console.log('Store=',this.props.mapState.markers.length)
      this.setState({
          markers: this.props.mapState.markers,
          routes: this.props.mapState.routes,
          location:{
            lat: this.props.mapState.location.lat,
            lng: this.props.mapState.location.lng
            },   
          zoom: this.props.mapState.zoom
      })
    }
/*
    componentDidUpdate(prevProps,prevState) {
        if(this.state.markers)
            console.log('WrapperMap componentDidUpdate state: '+this.state.markers.length)

        if(prevState.markers)
            console.log('WrapperMapcomponentDidUpdate prevState: '+prevState.markers.length)
    }
*/ 
    _messageUsuario = (opt, param) => {
        window.alert(`No se encontraron registros,  para la consulta solicitada
            con: ${this.state.arrQuerys[this.state.prevNivelQry]} = ${this.state.prevParam}
            y ${this.state.arrQuerys[opt]} = ${param}
            `)
    }

    _onSelectMarkers = param => {
        this.state.markers
            .filter(marker => marker.id === parseInt(param))
            .map(element => (
                this.setState( {
                    location:{
                        lat: element.ubicacion.geo.lat,
                        lng: element.ubicacion.geo.lng
                    },        
                    zoom: 15
            })
        ))
    }

    _onResetItems = () => {
        this.setState({
            markers: this.props.mapState.markers,
            location:{
                lat: this.props.mapState.location.lat,
                lng: this.props.mapState.location.lng
            },        
            zoom: this.props.mapState.zoom,
            prevParam: '',
            prevNivelQry:0
        })
    }
    
    _onHandleOfertaEducativa = param => {
        if(!param)
            return

        let opt = 1
        //let found =this.state.markers.filter(marker => marker.ofertaEducativa.find( (element) => element === param ) )
        //console.log('OfertaEducativa found=', found.length)
        this.setState({
            markers: this.props.mapState.markers.filter(marker => marker.ofertaEducativa.find( (element) => element === param ) ),        
            zoom: this.props.mapState.zoom,
            prevParam: param,
            prevNivelQry: opt
        })
    }

    _onHandleOfertaAcademica = param => {
        if(!param)
            return

        let opt = 2
        let found =this.state.markers.filter(marker => marker.ofertaAcademica.find( (element) => element === param ) )
        //console.log('OfertaAcademica found=', found.length)
        if(this.state.prevNivelQry >= opt){
            this.setState({
                markers: this.props.mapState.markers.filter(marker => marker.ofertaAcademica.find( (element) => element === param ) ),        
                zoom: this.props.mapState.zoom,
                prevParam: param,
                prevNivelQry: opt
            })
         }else if(found && found.length === 0){
            this._onResetItems()
            this._messageUsuario(opt, param)
        }else{    
            this.setState( (prevState) =>({
                markers: prevState.markers.filter(marker => marker.ofertaAcademica.find( (element) => element === param ) ),
                zoom: this.props.mapState.zoom,
                prevParam: param,
                prevNivelQry: opt
            }))
        }
    }

    _onHandleMunicipio = param => {
        if(!param)
            return

        let opt = 3
        let found =this.state.markers.filter(marker => marker.ubicacion.municipio === param)
       // console.log('Municipio found=', found.length, '  this.state.prevNivelQry= '+this.state.prevNivelQry+' prevparam='+this.state.prevParam+'='+param )
        if(this.state.prevNivelQry >= opt){
            this.setState({
                markers: this.props.mapState.markers.filter(marker => marker.ubicacion.municipio === param ),        
                zoom: this.props.mapState.zoom,
                prevParam: param,
                prevNivelQry: opt
            })
        }else if(found && found.length === 0){
            this._onResetItems()
            this._messageUsuario(opt, param)
        }else{ 
            this.setState( (prevState) =>({
                markers: prevState.markers.filter(marker => marker.ubicacion.municipio === param ),
                zoom: this.props.mapState.zoom,
                prevParam: param,
                prevNivelQry: opt
            }))
        }
    }
 
    render() {
        const position = [this.state.location.lat, this.state.location.lng]
        const { 
            markers,
            zoom,
            displayButtonRefreshMapa
        } = this.state
        const { 
            cataArrIconMarkers,
            cataArrIcons
        } = this.props.catalogoState
        const { 
            routes,
            url
        } = this.props.mapState

        return (
            <div>
                <NavbarInverse 
                    itemInicio='disabled'
                    _displayNavbarInicio = 'true'
                    _displayButtonConsultaMapa= {1}
                    _displayButtonRefreshMapa={1}
                    onResetItems = {this._onResetItems}
                    displayButtonRefreshMapa = {displayButtonRefreshMapa}
                />
                <FormConsultaModal
                    markers={markers}
                    onSelectMarkers = {this._onSelectMarkers.bind(this)}
                    onResetItems = {this._onResetItems}
                    onHandleOfertaEducativa={this._onHandleOfertaEducativa.bind(this)}
                    onHandleOfertaAcademica={this._onHandleOfertaAcademica.bind(this)}
                    onHandleMunicipio = {this._onHandleMunicipio.bind(this)}
                />
                <div className="map" style={_styles.divWrapper}>
                    <Map 
                        className='map' 
                        center={position} 
                        zoom={zoom} 
                        minZoom= '9'
                        maxZoom= '25'
                        zoomControl={false} 
                        bounds={this.state.bounds} 
                        maxBounds={this.state.bounds} 
                    >
                        <TileLayer
                            url={url}
                        />
                        <ZoomControl position="topright" />
                    
                        {markers.map(marker => (
                            <Marker
                            key={marker.id}
                            position={[marker.ubicacion.geo.lat, marker.ubicacion.geo.lng]}
                            title={marker.name}
                            icon={
                                L.icon({
                                    iconUrl: `/img/markers/${ cataArrIconMarkers[ marker.ofertaEducativa.slice(-1).pop() ] }`,
                                    iconSize: [44, 44],
                                    iconAnchor: [16.5, 44],
                                    popupAnchor: [0, -44]
                                })}
                           
                            >
                            <Popup>
                                <CardInfoWindow 
                                    name={marker.name}
                                    icon={cataArrIcons[marker.ofertaEducativa.slice(-1).pop()] }
                                    phone={marker.phone}
                                    email={marker.email}
                                    municipio= {marker.ubicacion.municipio}
                                    direccion={marker.ubicacion.direccion}
                                    url={marker.url} 
                                />
                            </Popup>
                            </Marker>
                        ))}
                        
                        <Polyline color="lime" weight="5" positions={routes} />
                        <Polyline color="white" weight="2" positions={routes} />
                    </Map>
                    
                </div>
                <FooterMap />
            </div>
        )
    }

}

const _styles = {
    divWrapper:{
        marginTop:'55px',
    },
    divWrapper_back:{
        width:'100%', 
        height:'900px', 
        marginTop:'55px',
    }

}

const mapStateToProps = state =>({
    mapState: state.mapState,
    catalogoState: state.catalogoState
})

export default connect(
    mapStateToProps,
    null
  )(WrapperMap);


