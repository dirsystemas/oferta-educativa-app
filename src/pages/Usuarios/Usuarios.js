import React, { Component } from 'react';

import NavbarInverse from '../components/NavbarInverse';
import Footer from '../components/Footer';
import TableUsuarios from './components/TableUsuarios';
import RecordAddUsuario from './components/RecordAddUsuario';
import RecordEditUsuario from './components/RecordEditUsuario';

class Usuarios extends Component {

  state = {
    user:{}
  }  
  
  //llamado desde Tableusuarios.js para actualizacion en RecordEditUsuarios
  _handleChange= (user) =>{
    //console.log('usuarios state',user)
    this.setState({ 
      user
    });
}
    
  render() {
            
    return (
        <div>
            <NavbarInverse 
              itemRegUsuarios='disabled'
              _displayButtonRegUsuarios= {1}
            />
            
            <main role="main" className="container" style={{marginTop:'80px', marginBottom:'60px'}}>
              <h1 className="mt-5">Pagina Registro Usuarios</h1>
              <RecordAddUsuario />
              <RecordEditUsuario
                user={this.state.user}
              />
              <TableUsuarios 
                _onChange = {this._handleChange}
              />
             
            </main>
           
            <Footer />
        </div>
    )
  }
}


export default Usuarios;