import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUsers } from '../../../negocio/actions/authActions';

class TableUsuarios extends Component {

    componentDidMount() {
        this.props.getUsers() 
    }

    render(){

        const { users } = this.props.users

        return(
            <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Email</th>
                    <th>Area</th>
                    <th></th>
                  </tr>                              
                </thead>
                <tbody>
                {users.map( (user, i) => (
                    <tr key={i}>
                        <td>{user.firstName} {user.lastName}</td>
                        <td>{user.userName}</td> 
                        <td>{user.email}</td>
                        <td>{user.area}</td> 
                        <td>
                        <button
                            type="button" className="btn btn-primary" data-toggle="modal" data-target="#formEditUsuarioModal"
                            
                            onClick={ ()=>this.props._onChange(user) }
                        >Editar</button>
                        </td>
                    </tr>
                ))}
                </tbody>              
            </table>
        )

    }

}

const mapStateToProps = state =>({
    users: state.authState
  })
  
  export default connect(
    mapStateToProps,
    {getUsers}
  ) (TableUsuarios);