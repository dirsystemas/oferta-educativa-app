import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import WrapperMap from './pages/Main/WrapperMap';
import Login from './pages/Login/Login';
import RegistroEscuelas from './pages/RegistroEscuelas/RegistroEscuelas';
import Usuarios from './pages/Usuarios/Usuarios';
import AcercaDe from './pages/AcercaDe/AcercaDe';
import Buzon from './pages/Buzon/Buzon';
import NoAuthority from './pages/components/NoAuthority';
import PageNotFound from './pages/components/PageNotFound';

class Navigator extends Component {
    
    render() {
      return (
        <Router>
            <Switch>
                <Route path='/' component={WrapperMap} exact/>
                <Route path='/records' component={RegistroEscuelas} />
                <Route path='/usuarios' component={Usuarios} />
                <Route path='/login' component={Login} />
                <Route path='/about' component={AcercaDe} />
                <Route path='/buzon' component={Buzon} />
                <Route path="/noAuthority" component={NoAuthority}/>
                <Route path='*' component={PageNotFound}/>
            </Switch>
        
        </Router>
      )
    }
  }
  
  export default Navigator;
