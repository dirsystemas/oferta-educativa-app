import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import Navigator from './Navigator';
import Negocio from './negocio/Negocio';



ReactDOM.render(
    <Provider store={Negocio}>
        <Navigator />
    </Provider>, 
    document.getElementById('root'));

